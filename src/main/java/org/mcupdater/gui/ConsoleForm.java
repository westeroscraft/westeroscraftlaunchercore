package org.mcupdater.gui;

import org.mcupdater.util.MCUpdater;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Level;

public class ConsoleForm {
	private CALogHandler consoleHandler;
	private ConsoleArea console;
	private final JFrame window;
	public final Container container; 

	public ConsoleForm(String title) {
        window = new JFrame();
        window.setIconImage(new ImageIcon(this.getClass().getResource("mcu-icon.png")).getImage());
        window.setTitle(title);
        window.setBounds(25, 25, 500, 500);
        window.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        container = window.getContentPane();
        buildContainer();
        window.setVisible(true);
	}
	private void buildContainer() {
        console = new ConsoleArea();
        container.setLayout(new BorderLayout());
        JScrollPane scroller = new JScrollPane(console, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        container.add(scroller, BorderLayout.CENTER);
        consoleHandler = new CALogHandler(console);
        consoleHandler.setLevel(Level.INFO);
        MCUpdater.apiLogger.addHandler(consoleHandler);
	}
	public ConsoleForm() {
	    container = new Container();
	    window = null;
	    buildContainer();
	}

	public ConsoleArea getConsole() {
		return console;
	}

	public CALogHandler getHandler() {
		return consoleHandler;
	}

	public void allowClose() {
	    if (window != null) {
	        window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	    }
	}
}
